             EMAIL-HTML-SENDER


Steps:

1.) installation of python on local computer - https://www.python.org/downloads/

2.) adding python script folder path in environment varibles from control panel - https://www.educative.io/answers/how-to-add-python-to-path-variable-in-windows

3.) setting up app password for gmail which will give a 16 digit password - https://support.google.com/mail/answer/185833?hl=en

4.) using sender email in EMAIL_ADDRESS variable and 16 digit password in EMAIL_PASSWORD from point 3.

5.) html file to be added will be added in between triple commas in .add_alternative class and its subtype will be html

6.) rest copy the script and run
